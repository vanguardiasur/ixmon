#ifndef __IXMON_H__
#define __IXMON_H__

#include <cstdio>

/*
 * The class always monitors "eth0". This can be modified
 * and set via a constructor parameter in the future, to
 * allow monitoring of multiple interfaces.
 */

class IXMon {
public:
	IXMon(char *ixname);
	~IXMon();

	/*
	 * Returns the fd used to read from "ip monitor",
	 * to allow polling on it.
	 */
	int get_fd();

	/*
	 * Try to read from the file descriptor and process
	 * pending events. Can be called at any time, even
	 * if it's unsure if there's data to read in the file
	 * descriptor.
	 */
	void proc_events();

	/* Returns "true" is the cable is connected on the interface */
	bool cable_connected();

	/* Prints the IP address/netmask into a char* buffer */
	void get_ip(char *p);
	void get_netmask(char *p);

	/* Returns true if network configurations appears to be OK */
	bool status();

private:
	int _fd;
	FILE *_mon;
	int _cable;
	unsigned _ip[4];
	unsigned _netmask; /* cidr */
	char _buf[1024];
	int _o;
	char _ix[32];

	void proc_one(char *p);
};

#endif
