                 IXMon: Interface monitoring

This class helps to monitor network events in an efficient way by
parsing the output of "ip monitor". Currently, it only processes link
status changes (cable connected/disconnected) and IP changes for any
interface.

USAGE
-----

After constructing an instance of the class, the monitoring is already
being done. You can call proc\_events() at any time to process all pending
network events and update the instance's status.

For constructing an instance, call the constructor with the name of the interface
as the only argument. For example:

	IXMon *m = new IXMon("eth0");

For efficient polling, you can call get\_fd() which will return the
file descriptor the class uses for reading, so you can monitor it via
select()/poll()/epoll() and only try to proc\_events() when reading from the 
fd is possible.

For reading the status:

  - cable\_connected(): will return "true" if the cable is connected.

  - get\_ip(p): will print the current IP address on the buffer pointed by p

  - get\_netmask(p): analogous to get\_ip, but for the netmask

  - status(): will return true if the interface appears to be properly running,
              meaning, the cable is connected and the IP is not zero.
