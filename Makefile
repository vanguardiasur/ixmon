.PHONY: all re clean
CXX ?= g++
CFLAGS := -Wall
LFLAGS :=

all: monitor

monitor: main.o IXMon.o
	$(CXX) $(LFLAGS) $^ -o $@

%.o: %.cpp
	$(CXX) $(CFLAGS) -c $<

re: clean all

clean:
	rm -f *.o
	rm -f monitor
