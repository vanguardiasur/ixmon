#include "IXMon.h"

#include <string.h>
#include <unistd.h>
#include <fcntl.h>

IXMon::IXMon(char *ix)
{
	int i;
	FILE *f;
	int m1,m2,m3,m4;
	int t;
	int flags;
	char b[200];

	strcpy(_ix, ix);

	/* Only monitor link and address changes */
	_mon = popen("ip monitor link addr", "r");
	if (!_mon)
		throw 1;

	_fd = fileno(_mon);

	/* Mark as non-blocking */
	flags = fcntl(_fd, F_GETFL, 0);
	flags |= O_NONBLOCK;
	fcntl(_fd, F_SETFL, flags);

	/* Read IP and netmask */
	sprintf(b, "ifconfig %s | grep 'inet addr'", _ix);
	f = popen(b, "r");
	if (!f) {
		for (i = 0; i < 4; i++)
			_ip[i] = 0;
		_netmask = 0;
	} else {
		t = fscanf(f, " inet addr: %i.%i.%i.%i Bcast:%*i.%*i.%*i.%*i Mask:%i.%i.%i.%i",
				&_ip[0], &_ip[1], &_ip[2], &_ip[3],
				&m1, &m2, &m3, &m4);
		if (8 == t) {
			_netmask = 32;
			m1 = m1 << 24 | m2 << 16 | m3 << 8 | m4;
			while (!(m1 & 1)) {
				m1 >>= 1;
				_netmask--;
			}
		} else {
			/* If reading failed, the interface may be unconfigured */
			for (i = 0; i < 4; i++)
				_ip[i] = 0;
			_netmask = 0;
		}
		fclose(f);
	}

	/* Read cable status */
	sprintf(b, "/sys/class/net/%s/carrier", _ix);
	f = fopen(b, "r");
	if (!f) {
		_cable = 0;
	} else {
		/* If reading fails, assume cable disconnected */
		if (1 != fscanf(f, "%i", &_cable))
			_cable = 0;

		fclose(f);
	}

	_o = 0;

	proc_events();
}

IXMon::~IXMon()
{
	fclose(_mon);
}

int IXMon::get_fd()
{
	return _fd;
}

void IXMon::proc_events()
{
	int t = 0;
	char *p, *pp;
	int l;

	while (t = read(_fd, _buf + _o, sizeof _buf - 1 - _o), t > 0)
		_o += t;

	pp = _buf;
	/* Process all lines */
	while (p = strchr(pp, '\n'), p != NULL) {
		*p = 0;

		proc_one(pp);
		pp = p + 1;
	}

	l = pp - _buf;
	memmove(_buf, _buf + l, _o - l);
	_o -= l;
}

void IXMon::proc_one(char *p)
{
	int u; /* unused placeholder */
	char state[100];
	char ix[32];
	int ip1, ip2, ip3, ip4, nm;

	if (p[0] == ' ') {
		/*
		 * Skip lines that start with a space, since
		 * they're remnants of previous lines. For now,
		 * we don't need to read any of that data
		 */
		return;
	}

	if (6 == sscanf(p, "%*i: %s inet %i.%i.%i.%i/%i", ix,
				&ip1, &ip2, &ip3, &ip4, &nm)) {
		if (strcmp(ix, _ix))
			return;

		_ip[0] = ip1;
		_ip[1] = ip2;
		_ip[2] = ip3;
		_ip[3] = ip4;
		_netmask = nm;
	} else if (2 == sscanf(p, "Deleted %*i: %s inet %*i.%*i.%*i.%*i/%i", ix, &u)) {
		int i;

		if (strcmp(ix, _ix))
			return;

		for (i = 0; i < 4; i++)
			_ip[i] = 0;
		_netmask = 0;
	} else if (2 == sscanf(p, "%*i: %[^:]: <%*[^>]> mtu %*s qdisc %*s state %s", ix, state)) {
		if (strcmp(ix, _ix))
			return;

		if (!strcmp(state, "UP")) {
			_cable = 1;
		} else {
			_cable = 0;
		}
	} else {
		printf("Unrecognized event: <%s>\n", p);
	}
}

bool IXMon::status()
{
	/* Check for cable connected and not null IP */
	return _cable && (_ip[0] || _ip[1] || _ip[2] || _ip[3]);
}
