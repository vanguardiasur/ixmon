#include "IXMon.h"

#include <stdio.h>
#include <unistd.h>
#include <sys/select.h>

int main() {
	IXMon *m = new IXMon((char*)"eth0");
	fd_set s;

	FD_ZERO(&s);
	FD_SET(m->get_fd(), &s);

	printf("%i\n", m->status());
	while (1) {
		select(m->get_fd() + 1, &s, NULL, NULL, NULL);
		m->proc_events();
		printf("%i\n", m->status());
	}

	return 0;
}
